
TARGETS := gxpp gxpd gxpm

all: $(TARGETS)

CFLAGS := -Wall -g
$(TARGETS): gxp-int.c
$(TARGETS): CFLAGS += -I/usr/include/libxml2
$(TARGETS): LOADLIBES := -lxml2 -lm

install: all
	for i in $(TARGETS); do \
		install -D $$i $(DESTDIR)/usr/bin/$$i; \
		install -D $$i.1 $(DESTDIR)/usr/share/man/man1/$$i.1; \
	done

clean:
	$(RM) $(TARGETS)


VERSION := $(shell awk '/^Version:/ { print $$2 }' gxpp.spec)

ifdef DIST
  RPMDEFS := -D "dist $(DIST)"
endif

tarfiles := gxpp.spec Makefile
tarfiles += $(patsubst %,%.c,$(TARGETS))
tarfiles += gxp-int.c gxp-int.h
tarfiles += $(patsubst %,%.1,$(TARGETS))

tarball := gxpp-$(VERSION).tar.gz

$(tarball): $(tarfiles)
	-$(RM) $@
	git archive --format=tar --prefix=gxpp-$(VERSION)/ HEAD^{tree} | gzip -9 > $@

tarball: $(tarball)

rpm: $(tarball) $(tarfiles)
	rpmbuild -ta $< $(RPMDEFS)

srpm: $(tarball) $(tarfiles)
	rpmbuild -ts $< $(RPMDEFS)
