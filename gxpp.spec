Name:           gxpp
Version:        1.3
Release:        2%{?dist}
Summary:        Simple XPath command line tools

Group:          Applications/Text
License:        GPL
Buildroot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)
Source0:        gxpp-%{version}.tar.gz
BuildRequires:  libxml2-devel
BuildRequires:  gcc


%description
This is a set of simple but powerful tools to query and manipulate
XML files using XPath expressions.

%prep
%setup -q


%build
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
/usr/bin/gxpp
/usr/bin/gxpd
/usr/bin/gxpm
%doc %{_mandir}/man?/*


%changelog
* Wed Apr 21 2021 Nathan Straz <nstraz@redhat.com> 1.3-2
- Add gcc to BuildRequires (nstraz@redhat.com)
- Add releaser for rhel9 (nstraz@redhat.com)
- Add releaser for qe-rhel7 (nstraz@redhat.com)

* Mon Jul 02 2018 Nathan Straz <nstraz@redhat.com> 1.3-1
- Switch to gzip tarballs for tito (nstraz@redhat.com)

* Mon Jul 02 2018 Nathan Straz <nstraz@redhat.com> 1.2-1
- new package built with tito

* Tue Apr 21 2009 Nate Straz <nstraz@redhat.com> 1.1-1
- Include fix for NULL nodesetval 

* Mon Aug 04 2008  1.0-1
- Initial packaging
