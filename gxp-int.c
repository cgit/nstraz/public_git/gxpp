/*
 * Copyright © 2007  Red Hat, Inc. All rights reserved.
 *
 * This copyrighted material is made available to anyone wishing to use,
 * modify, copy, or redistribute it subject to the terms and conditions of the
 * GNU General Public License v.2.  This program is distributed in the hope
 * that it will be useful, but WITHOUT ANY WARRANTY expressed or implied,
 * including the implied warranties of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. Any Red Hat
 * trademarks that are incorporated in the source code or documentation are not
 * subject to the GNU General Public License and may only be used or replicated
 * with the express permission of Red Hat, Inc.
 *
 * Red Hat Author(s): Nathan Straz <nstraz@redhat.com>
 */

#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

xmlXPathContextPtr 
generate_context(xmlDocPtr doc, const char *defprefix)
{
	xmlXPathContextPtr ctx;
	xmlNodePtr root;
	xmlNsPtr ns;

	root = xmlDocGetRootElement(doc);
	ctx = xmlXPathNewContext(doc);

	for (ns = root->ns; ns; ns=ns->next) {
		if (ns->href) {
			if (ns->prefix) {
				xmlXPathRegisterNs(ctx, ns->prefix, ns->href);
			} else if (defprefix) {
				xmlXPathRegisterNs(ctx, (xmlChar *)defprefix, ns->href);
			} else {
				fprintf(stderr, "WARNING: document uses default namespace, use -p option to specify a prefix\n");
			}
		}
	}

	return ctx;
}
